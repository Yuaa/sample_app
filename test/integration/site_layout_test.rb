require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
    def setup
        @user = users(:michael)
    end

    test "layout links" do
            
        get root_path
        assert_template 'static_pages/home'
        assert_select "a[href=?]", root_path, count: 2
        assert_select "a[href=?]", help_path
        assert_select "a[href=?]", about_path
        assert_select "a[href=?]", contact_path
        get signup_path
        assert_select "title",  full_title("Sign up")
        get login_path
        assert_select "title",  full_title("Log in")

        log_in_as(@user)
        get root_path
        assert_select "a[href=?]", root_path, count: 2
        get help_path
        assert_select "a[href=?]", help_path
        assert_template 'static_pages/help'
        get about_path
        assert_select "a[href=?]", about_path
        assert_template 'static_pages/about'
        get contact_path
        assert_select "a[href=?]", contact_path
        assert_template 'static_pages/contact'

        delete logout_path
        assert_not is_logged_in?
        assert_redirected_to root_url
        follow_redirect!
        assert_select "a[href=?]", login_path
        assert_select "a[href=?]", logout_path, count: 0
        assert_select "a[href=?]", user_path(@user), count: 0

        session.delete(:user_id)
        @current_user = nil

        get root_path
        assert_select "a[href=?]", root_path, count: 2
        get help_path
        assert_select "a[href=?]", help_path
        assert_template 'static_pages/help'
        get about_path
        assert_select "a[href=?]", about_path
        assert_template 'static_pages/about'
        get contact_path
        assert_select "a[href=?]", contact_path
        assert_template 'static_pages/contact'


    end        
end    